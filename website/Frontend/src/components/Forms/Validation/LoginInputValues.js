import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

let notify = (info, durInMilliSec) => {
    toast.notify(info, {duration: durInMilliSec});
}

let validate = (event, fromLogin, fromRegister) => {
    if (event.target.username.value === "") {
        notify("Username can't be empty", 4000);
        return false;
    }

    if (fromLogin || fromRegister) {
        if (event.target.password.value === "") {
            notify("Invalid Password", 4000);
            return false;
        }
    }

    if ((!fromLogin && !(event.target.password.value === "")) || fromRegister) {

        if (!/([A-Z]+)/g.test(event.target.password.value) ||
            !/([a-z]+)/g.test(event.target.password.value) || 
            event.target.password.value.length < 8 ) {

            notify("Allowed passwords include: "+
                "At least 8 characters, one upper case, one lower case, letters and/or numbers",
                null);
            notify("Invalid Password", 4000);
            return false;
        }
    }

    return true;
}

export default {validate};