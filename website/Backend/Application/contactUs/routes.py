from flask import Blueprint, request
from Application.user.utils import response
from Application.contactUs.utils import send_contact_email
from flask_cors import cross_origin
import pprint

pp = pprint.PrettyPrinter(indent=4)

contactUs = Blueprint('contactUs', __name__)


@contactUs.route("/contactUs", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def contactUsEmail():
    if request.method == "POST":
        
        content = request.get_json()
        if send_contact_email(content.get("inquiry"), content.get("user_email_address"), "user"):
            # replace the recepient with one of the team member's email
            if send_contact_email(content.get("inquiry"), "kushalpachchigar123@gmail.com", "master"):
                return response({
                    "status": True
                })
            else:
                return response({ "status":False, "message": "Error sending the Inquiry to our team member -- Sorry"})
        else:
            return response({ "status":False, "message": "Error sending the confirmation Email. Please verify your email and send a new inquiry"})
    else:
        return response({
            "message": "Don't try to act smart using the api to act like a user! To do so, use the website instead"
        })