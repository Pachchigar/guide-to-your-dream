import React from 'react';

import { Col, Row, Container, Figure } from "react-bootstrap";

import toast from 'toasted-notes';
import 'toasted-notes/src/styles.css';
import "../../css/Profile.css";

import DisplayFiles from './DisplayFiles';

import MasterQueryForm from '../Forms/MasterQueryForm';

class MasterDocumentsPage extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
          user: {},
          loggedIn: false,
      };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    tryImage = () => {
        var src = ""

        if (this.state.user.user_image) {
            src = "/images/Profile_Pictures/"+this.state.user.user_image
        }
        
        return <Figure.Image
                    className="logo-hover"
                    src={src}
                    alt={this.state.user.first_name}
                    width={275}
                />
    }

    displayUserDetails = () => {
        if (this.state.user.username) {
            return (
                <Container>
                    <Row>
                        <Col>
                            <div className="profile_pic mx-auto">
                                <this.tryImage />
                            </div>
                        </Col>

                        <Col className="user_details">
                            <h3>Documents for: {this.state.user.first_name} {this.state.user.last_name}</h3>
                            <h5>Contact info: {this.state.user.username}, {this.state.user.contact}</h5>
                        </Col>
                    </Row>
                </Container>
            );
        } 
        return <></>
    }

    onClickUpload = (data) => {
        this.setState({ user: data })
    }

    render() {
        
        return (
            <div>
                <Row>
                    <Col style={{ paddingTop: "2%" }}>
                        <header>
                            <h1 className="text-dark text-center">Documents</h1>
                        </header>
                    </Col>
                </Row>

                <Container>
                    <MasterQueryForm onClickUpload={this.onClickUpload}/>
                    <DisplayFiles currentUser={this.state.user} onClickUpload={this.onClickUpload}/>
                    <this.displayUserDetails />
                </Container>
            </div>
        );
    }
}

export default MasterDocumentsPage;