import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import Validate from "./ValidateFields"


let validate = (event, fromRegister) => {
    
    if (event.target.first_name.value === "") {
        toast.notify("First Name can't be empty", 4000);
        return false;
    }

    if (event.target.last_name.value === "") {
        toast.notify("Last Name can't be empty", 4000);
        return false;
    }

    if (event.target.contact.value.length < 10 || 
            event.target.contact.value.length > 15) {
        toast.notify("Invalid contact number", 4000);
        return false;
    }

    // fromLogin is false clearly because this is called from registration form
    if (!Validate.validateLoginInputValues(event, false, fromRegister)) {
        return false
    }
    
    if ((event.target.password.value !== "") || 
        !event.target.password_confirmation.value === "" || 
        fromRegister) {

        if (!/([A-Z]+)/g.test(event.target.password_confirmation.value) ||
            !/([a-z]+)/g.test(event.target.password_confirmation.value) || 
            event.target.password_confirmation.value.length < 8 ||
            event.target.password.value !== event.target.password_confirmation.value) {

            toast.notify("Invalid Re-enter Password", 4000);
            return false;
        }
    }

    return true;
}

export default {validate};