import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import RegistrationFields from "./RegistrationFields";

import Validate from "./Validation/ValidateFields";
import axios from "axios";
import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';
import { Button } from "react-bootstrap";

class ResetProfileForm extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
          user: {},
          profilePic: null,
      };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    componentDidMount() {
        if (this.props.user !== {}) {
            this.setState({user: this.props.user})
        }
    }

    makePostRequest = (data, infoFrom, url, contentType) => {
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'content-type': contentType,
                    }
                },
                withCredentials: true
            })
            .then(response => {
                console.log("Post " + infoFrom + ": ", response);
                
                if (response.data.status) {
                    this.notify(infoFrom + " successful!", 3000);
                    this.props.onClickUpdate(response.data)
                } else {

                    // notify appropriate msg
                    if (response.data.message === "Nothing Updated" ||
                            response.data.message === "No profile picture found") {
                        this.notify(response.data.message, 6000);
                    } else {
                        response.data.message.map( (msg) => {
                            this.notify(msg, 6000);
                            return msg
                        })
                    }
                }
            })
            .catch(error => {
                console.log("Post "+ infoFrom +" error: ", error);

                this.notify("Error during "+ infoFrom, 4000);
            }
        );
    }

    UpdateHandler = (event) => {
        event.preventDefault();
        
        // fromRegister parameter is flase as this is fromUpdate
        if (Validate.validateRegistrationInputValues(event, false)) {
            const data = {
                first_name: event.target.first_name.value,
                last_name: event.target.last_name.value,
                contact: event.target.contact.value,
                username: event.target.username.value,
                password: event.target.password.value,
                password_confirmation: event.target.password_confirmation.value
            }
            var url = "http://localhost:5000/update"


            var profileData = new FormData();
            profileData.append("data", JSON.stringify(data))
            if (this.state.profilePic !== null) {
                profileData.append("user_image", this.state.profilePic)
            }

            // to update user details in the db
            this.makePostRequest(profileData, "Update", url, 'multipart/form-data');

        }
    }

    handleImage = (event) => {
        if (event.target.files.length) {
            this.setState({ profilePic: event.target.files[0]})
          }
    }

    deleteProfilePicture = () => {
        const url = "http://localhost:5000/deleteProfilePic"
        this.makePostRequest({}, "Deletion", url, 'application/json');
    }

    addResetProfileForm = () => {
        return (
            <form name="registrationform"
                className="form-inline active-cyan-4 my-2"
                onSubmit={this.UpdateHandler}
            >

            <div>

                <RegistrationFields 
                    first_name={this.state.user.first_name}
                    last_name={this.state.user.last_name}
                    contact={this.state.user.contact}
                    username={this.state.user.username}
                />
                
                <label >
                    <div style={{ margin: "10px 0px 20px 120px " }}>
                        Your profile picture: 
                    </div>
                    <input
                        type="file"
                        name="profile_pic_input"
                        style={{ margin: "10px 0px 20px 20px " }}
                        onChange={this.handleImage}
                    />
                </label>

                <label>
                    <Button
                        onClick={this.deleteProfilePicture}
                        variant="outline-secondary"
                    >
                        Delete Profile Picture
                    </Button>
                </label>

                <div style={{textAlign: "center"}}>
                    <input 
                        className="btn-primary "
                        style={{ boxShadow: "5px 5px 30px -10px black", marginTop: "20px" }}
                        type="submit" 
                        value="Update" 
                    />
                </div>
            </div>
            
            </form>
        );
    }

    addTab = () => {
        return (
            <Tabs>
                <TabList>
                    <Tab>Profile details</Tab>
                </TabList>
                <TabPanel></TabPanel>
            </Tabs>
        );
    }

    render() {
        if (this.state.user !== {}) {
            return (
                <div>
                    <this.addTab />
                    <this.addResetProfileForm />
                </div>
            );
        }
        return <></>
    }
}

export default ResetProfileForm;