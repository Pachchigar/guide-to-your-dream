import React from 'react';

import LoginFields from "./LoginFields";

class RegistrationFields extends React.Component {

    render() {
        var first_name_value=""
        var last_name_value=""
        var contact_value=""
        var username_value=""

        if (this.props.first_name) {
            first_name_value=this.props.first_name
            last_name_value=this.props.last_name
            contact_value=this.props.contact
            username_value=this.props.username
        }
        
        return (
            <>
            <label>
                First Name:
                <input 
                    name="first_name"
                    className="form-control" 
                    style={{ boxShadow: "5px 5px 15px -12px black", margin: "10px 20px 10px 5px" }}
                    type="text" 
                    placeholder="First Name" 
                    onChange={this.handleChange} 
                    defaultValue={first_name_value}
                />

                Last Name:
                <input 
                    name="last_name"
                    className="form-control" 
                    style={{ boxShadow: "5px 5px 15px -12px black", margin: "10px 20px 10px 5px" }}
                    type="text" 
                    placeholder="Last Name" 
                    onChange={this.handleChange} 
                    defaultValue={last_name_value}
                />
            </label>

            <label>
                Contact Number:
                <input 
                    name="contact"
                    className="form-control" 
                    style={{ boxShadow: "5px 5px 15px -12px black", margin: "10px 0px 10px 10px" }}
                    type="integer" 
                    placeholder="ex: +9119876543219" 
                    onChange={this.handleChange} 
                    defaultValue={contact_value}
                />
            </label>

            {/* add email and password textareas */}
            <LoginFields username_value={username_value}/>

            <label>
                Re-enter<br/> Password:
                <input 
                    name="password_confirmation"
                    className="form-control" 
                    style={{ boxShadow: "5px 5px 15px -12px black", margin: "20px 0px 20px 62px " }}
                    type="password" 
                    placeholder="*******" 
                    onChange={this.handleChange} 
                />
            </label>
            </>
        );
    }
}

export default RegistrationFields;