import React from "react";

class Footer extends React.Component {
  render() {
    const style = {
      backgroundColor: "#f6f9fc",
      // margin: "0px 0 0 0",
      padding: "10px 0",
      color: "#8898aa",
    };

    return (
      <footer style={style}>
        <div className="container footer">
          <div className="row text-center">
            <div className="col-3">
              <h6>Visa Helper &copy;</h6>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
