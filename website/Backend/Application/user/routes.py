from flask import Blueprint, request
from Application.user.utils import (validPassword, validusername, 
                                        validContact, response, updateDB, 
                                        send_reset_email,
                                        getUserFiles)
from Application import bcrypt, db, app
from Application.models import User, File
from flask_login import login_user, current_user, logout_user
from flask_cors import cross_origin
import os
import pprint

pp = pprint.PrettyPrinter(indent=4)

users = Blueprint('users', __name__)



@users.route("/register", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def register():
    if request.method == "POST":
        content = request.get_json()

        # prints on the command prompt not the browser console!
        # pp.pprint(content)

        msg = ""

        # add to the db if conditions are met and return the details except for password
        # password === confirmationPassword
        # valid username i.e valid email (taken care of in react frontend)
        # userName not taken
        # contact not taken
        if (validPassword (content.get("password"), content.get("password_confirmation"))):
            if (validusername(content.get("username"))):
                if (validContact(content.get("contact"))):
                    # conditions are met => add it to db
                    hashed_password = bcrypt.generate_password_hash(content.get("password")).decode('utf-8')
                    user = User(
                        first_name=content.get("first_name"), 
                        last_name=content.get("last_name"),
                        contact=content.get("contact"), 
                        username=content.get("username"), 
                        password=hashed_password
                    )
                    db.session.add(user)
                    db.session.commit()
                    login_user(user, True)
                    # return necessary details
                    return response({
                        "status": True,
                        "id": db.session.query(User).filter_by(username=user.username).first().id,
                        "first_name": user.first_name,
                        "last_name": user.last_name,
                        "contact": user.contact,
                        "username": user.username,
                        "user_image": "",
                        "files": ""
                    })
                else:
                    msg = "A user with contact number already exists"
            else:
                msg = "username already exists: Enter a different username"
        else:
            msg = "invalid password confirmation"
        
        # registration failed
        # add what failed the validation
        return response({"status": False, "message": msg})
    else:
        return response({"message": "Don't try to act smart using the api to register a user! Use the website instead"})






@users.route("/login", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def login():
    if request.method == "POST":
        user = current_user
        if (user and (user.is_authenticated)):
            return response({"status": False, "message": "Another user already logged in. Logout first"})

        content = request.get_json()

        # prints on the command prompt not the browser console!
        # pp.pprint(content)

        # verify if the user exists with the given userName and compare the hashed password
        # if met, then return the user details except for password
        user = User.query.filter_by(username=content.get("username")).first()
        if user and bcrypt.check_password_hash(user.password, content.get("password")):
            login_user(user, True)

            files = getUserFiles(user.files)

            # return necessary user details
            return response({
                "status": True,
                "id": user.id,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "contact": user.contact,
                "username": user.username,
                "user_image": user.user_image,
                "files": files
            })
        else:
            # Login failed
            return response({"status": False, "message": "invalid credentials -- New user? Register first!"})
    else:
        return response({"message": "Don't try to act smart using the api to get user info! Login on the website instead"})






@users.route("/currentLogin", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def currentLogin():
    # prints on the command prompt
    # pp.pprint(current_user)

    if request.method == "POST":
        user = current_user
        if (user and (user.is_authenticated)):

            files = getUserFiles(user.files)

            return response({
                "status": True,
                "id": user.id,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "contact": user.contact,
                "username": user.username,
                "user_image": user.user_image,
                "files": files
            })
        else:
            return ({ "status": False, "message": "No user currently logged in!" })
    else:
        return response({"message": "Don't try to act smart using the api to get user info!"})






@users.route("/logout", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def logout():
    if request.method == "POST":
        if (current_user and current_user.is_authenticated):
            logout_user()
            return response({
                "status": True
            })
        else:
            return response({"status": False, "message": "No User currently Logged in"})
    else:
        return response({
            "message": "Don't try to act smart using the api to logout a user! To logout yourself, use the website instead"
        })






@users.route("/update", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def update():
    if request.method == "POST":
        content = eval(request.form.get("data"))
        user = current_user
        msg = []
        if (user and (user.is_authenticated)):
            # update the DB if conditions met
            if (updateDB(content, user, msg)):

                files = getUserFiles(user.files)

                return response({
                    "status": True,
                    "id": user.id,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "contact": user.contact,
                    "username": user.username,
                    "user_image": user.user_image,
                    "files": files
                })
            else:
                if len(msg) == 0:
                    return response({ "status": False, "message": "Nothing Updated" })
                else:
                    return response({ "status": False, "message": msg })
        else:
            return response({ "status": False, "message": "No user currently logged in!" })
    else:
        return response({
            "message": "Don't try to act smart using the api to update a user! To update your profile, use the website instead"
        })






@users.route("/deleteProfilePic", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def deleteProfilePic():
    if request.method == "POST":
        user = current_user
        if (user and (user.is_authenticated)):
            # update the DB 
            if (user.user_image != "default.jpg"):
                user_image_name = user.user_image
                current_user.user_image = "default.jpg"
                
                
                deletepath = os.path.join(app.root_path, 'static/Profile_Pictures', user_image_name)
                if os.path.exists(deletepath):
                    os.remove(deletepath)
                    db.session.commit()
                    files = getUserFiles(user.files)

                    return response({ 
                            "status": True,
                            "id": user.id,
                            "first_name": user.first_name,
                            "last_name": user.last_name,
                            "contact": user.contact,
                            "username": user.username,
                            "user_image": user.user_image,
                            "files": files 
                    })
                else:
                    return response({ "status": False, "message": "No profile picture found" })
            else:
                return response({ "status": False, "message": "No profile picture found" })
        else:
            return response({ "status": False, "message": "No user currently logged in!" })
    else:
        return response({
            "message": "Don't try to act smart using the api to delete a user Profile Picture! To update your profile, use the website instead"
        })






@users.route("/deleteAccount", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def deleteAccount():
    if request.method == "POST":
        if (current_user and current_user.is_authenticated):
            username = current_user.username

            # delete dependency files 
            deleteProfilePic()

            for user_file in current_user.files: 
                deletepath = os.path.join(app.root_path, 'static/Documents', user_file.file_name)
                if os.path.exists(deletepath):
                    os.remove(deletepath)
                    # delete file record from db
                    db.session.query(File).filter_by(id=user_file.id).delete(synchronize_session=False) 
            db.session.commit()

            logout_user()

            # delete the data from db 
            db.session.query(User).filter_by(username=username).delete(synchronize_session=False) 
            db.session.commit()
            return response({
                "status": True
            })
        else:
            return response({
                "status": False,
                "messade": "No user currently logged in!"
            })
    else:
        return response({
            "message": "Don't try to act smart using the api to delete a user Account! To do so, use the website instead"
        })






@users.route("/resetPassword", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def resetPasswordLink():
    if request.method == "POST":
        user = current_user
        if (user and (user.is_authenticated)):
            return response({"status": False, "message": "Another user already logged in. Logout first"})

        content = request.get_json()
        user = User.query.filter_by(username=content.get("username")).first()
        if user:
            # send the email with token and reset link
            send_reset_email(user)
            # then return status as True
            return response({
                "status": True
            })
        else:
            return response({
                "status": False,
                "message": "No account with the given Email exists. Register a new account instead"
            })
    else:
        return response({
            "message": "Don't try to act smart using the api to update a user Account! To do so, use the website instead"
        })






@users.route("/resetPassword/<token>", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def resetPassword(token):
    if request.method == "POST":
        user = current_user
        if (user and (user.is_authenticated)):
            return response({"status": False, "message": "Another user already logged in. Logout first"})

        content = request.get_json()
        # pp.pprint(content.get("password"))

        # if valid token, then store the hashed password in the user table
        user = User.verify_reset_token(token)
        if user:
            if bcrypt.check_password_hash(user.password, content.get("password")):
                return response({"status": False, "message": "Password already in use!"})

            user.password = bcrypt.generate_password_hash(content.get("password")).decode('utf-8')
            db.session.commit()
            return response({
                "status": True
            })
        else:
            return response({"status": False, "message": "Invalid or Expired token. Click on forget password again to send a new reset link!"})
    else:
        return response({
            "message": "Don't try to act smart using the api to update a user Account! To do so, use the website instead"
        })