from flask import Blueprint, request, send_from_directory
from flask_login import current_user
from Application.user.utils import response, allowed_file, saveDocument, getUserFiles
from Application.models import File, User
from Application import db, app
from flask_cors import cross_origin
import os

files = Blueprint('files', __name__)


@files.route("/uploadDoc", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def uploadDoc():
    if request.method == "POST":
        user = current_user
        if (user and not user.is_authenticated):
            return response({"status": False, "message": "Login First"})
            
        content = eval(request.form.get("data"))
        content_file = request.files.get("docFile")
        
        if(content_file):
            if allowed_file(content_file.filename, {'pdf'}):

                # store the file in static/Documents folder
                filename = saveDocument(content_file)

                # store metadata in the db File Table
                fileToStore = File(
                    file_description=content.get("file_description"), 
                    file_name=filename,
                    file_name_to_display=content_file.filename,
                    user_id=user.id
                )
                db.session.add(fileToStore)
                db.session.commit()

                files = getUserFiles(user.files)

                return response({
                    "status": True,
                    "id": user.id,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "contact": user.contact,
                    "username": user.username,
                    "user_image": user.user_image,
                    "files": files 
                })
            else:
               return response({ "message": "File type must be 'pdf'", "status": False })
        else: 
            return response({ "message": "Error: No file received to upload in backend", "status": False })
    else:
        return response({
            "message": "Don't try to act smart using the api to update a user Account! To do so, use the website instead"
        })






@files.route("/deleteDocument", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def deleteDocument():
    if request.method == "POST":
        user = current_user
        if (user and not user.is_authenticated):
            return response({"status": False, "message": "Login First"})
        
        content = request.get_json()
        file_id = content.get("file_id")
        if (file_id):
            toDeleteFile = db.session.query(File).filter_by(id=file_id).first()
            
            if (toDeleteFile):
                # delete file record from db
                db.session.query(File).filter_by(id=file_id).delete(synchronize_session=False) 

                # delete file from file system 
                deletepath = os.path.join(app.root_path, 'static/Documents', toDeleteFile.file_name)
                if os.path.exists(deletepath):
                    os.remove(deletepath)

                
                    db.session.commit()

                    files = getUserFiles(user.files)
                    return response({
                        "status": True,
                        "id": user.id,
                        "first_name": user.first_name,
                        "last_name": user.last_name,
                        "contact": user.contact,
                        "username": user.username,
                        "user_image": user.user_image,
                        "files": files 
                    })
            else:
                return response({
                    "status": False,
                    "message": "No file to delete on storage"
                })
        else:
            return response({
                "status": False,
                "message": "No file to delete"
            })
    else:
        return response({
            "message": "Don't try to act smart using the api to update a user Account! To do so, use the website instead"
        })






@files.route("/downloadDoc", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def downloadDoc():
    if request.method == "POST":
        user = current_user
        if (user and not user.is_authenticated):
            return response({"status": False, "message": "Login First"})
        
        content = request.get_json()
        file_id = content.get("file_id")
        if (file_id):
            user_file = db.session.query(File).filter_by(id=file_id).first()
            folderPath = os.path.join(app.root_path, 'static/Documents')
            return send_from_directory(folderPath, user_file.file_name, attachment_filename=user_file.file_name_to_display, as_attachment=True)
        else:
            return response({
                "status": False,
                "message": "No document to download"
            })
    else:
        return response({
            "message": "Don't try to act smart using the api to get a user info! To do so, use the website instead"
        })





@files.route("/masterDocuments", methods=["GET", "POST", "OPTIONS"])
@cross_origin(supports_credentials=True)
def masterDocuments():
    if request.method == "POST":
        user = current_user
        if (user and not user.is_authenticated):
            return response({"status": False, "message": "Login First"})

        if (user.username == "kushalpachchigar@utexas.edu"):
            content = request.get_json()
            return_user = None

            if (content.get("username") != ""):
                return_user = db.session.query(User).filter_by(username=content.get("username")).first()
                if (return_user):
                    if (content.get("contact") != ""):
                        if (return_user.contact != content.get("contact")):
                            return response({"status": False, "message": "contact number not the same as registered"})
                else:
                    return response({"status": False, "message": "No user with the given username exists"})
            elif (content.get("contact") != ""):
                return_user = db.session.query(User).filter_by(contact=content.get("contact")).first()
                if (return_user):
                    if (content.get("username") != ""):
                        if (return_user.username != content.get("username")):
                            return response({"status": False, "message": "username not the same as registered"})
                else:
                    return response({"status": False, "message": "No user with the given contact number exists"})
            else:
                return response({"status": False, "message": "No information to make the master query"})


            if (return_user and return_user.username):
                files = getUserFiles(return_user.files)
                return response({
                    "status": True,
                    "id": return_user.id,
                    "first_name": return_user.first_name,
                    "last_name": return_user.last_name,
                    "contact": return_user.contact,
                    "username": return_user.username,
                    "user_image": return_user.user_image,
                    "files": files 
                })
        else:
            return response({"status": False, "message": "Forbidden access -- Logged in is not a master account"})
    else:
        return response({
            "message": "Don't try to act smart using the api to get a user info! To do so, use the website instead"
        })