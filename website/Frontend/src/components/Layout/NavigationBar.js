import React from "react";
import { Nav, Navbar, NavDropdown } from "react-bootstrap";

import LoginForm from "../Forms/LoginForm";

// To load the popup screen for login page
import { Modal } from 'react-responsive-modal';

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import axios from "axios";


class NavigationBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      openLoginModal: false,
    };
  }

  notify = (info, durInMilliSec) => {
    toast.notify(info, {duration: durInMilliSec});
  }

  onOpenModal = () => {
    this.setState({ openLoginModal: true });
  };
 
  onCloseModal = () => {
      this.setState({ openLoginModal: false });
  };

  onLogin = (data) => {
    this.onCloseModal();
    this.props.logIn(data);
  }

  // make the post call to update the backend that a user has logged out
  onLogOut = () => {
    const url = "http://localhost:5000/logout"
    const data = {}
    axios(
      {
          method: 'POST',
          url,
          data: data,
          config: {
              headers: {
                  'Content-Type': 'application/json',
              }
          },
          withCredentials: true
      })
      .then(response => {
          console.log("Post logout: ", response);
          
          if (response.data.status) {
            this.props.logOut();
          } else {
            this.notify("logout Unsuccesful", 4000);
          }
          
      })
      .catch(error => {
          console.log("Post logout error: ", error);

          this.notify("Error during logout", 4000);
      }
    );
  }

  linkLogin = () => {
    return (
      <Modal open={this.state.openLoginModal} onClose={this.onCloseModal} center>
        <LoginForm  onLogin={this.onLogin}/>
      </Modal>
    );
  }

  addLog = () => {
    if (!this.props.loggedInStatus) {
      return <Nav.Link onClick={this.onOpenModal}>Login | Register</Nav.Link>
    } else {
      var docUrl = "/myAccount/documents"
      if (this.props.user && this.props.user.username === "kushalpachchigar@utexas.edu") {
        // master account
        docUrl = "/myAccount/masterDocuments"
      }

      return (
        <NavDropdown title="My Account" id="collasible-nav-dropdown">
          <NavDropdown.Item href="/myAccount/profile">Profile</NavDropdown.Item>
          <NavDropdown.Item href={docUrl}>Documents</NavDropdown.Item>

          <NavDropdown.Divider />
          
          <NavDropdown.Item onClick={this.onLogOut}>Log out</NavDropdown.Item>
        </NavDropdown>
      );
    }
  }

  render() {
    return (
      <div>
        <nav>
            <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
            <Navbar.Brand href="/">Visa Helper</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="ml-auto">
                  <Nav.Link href="/eligibility">Eligibility</Nav.Link>
                  <Nav.Link href="/pricing">Pricing</Nav.Link>
                  <Nav.Link href="/contact">Contact us</Nav.Link>
                  <this.addLog /> 
                  <Nav.Link href="/about">About</Nav.Link>
              </Nav>
            </Navbar.Collapse>
            </Navbar>
        </nav>
        <this.linkLogin/>
      </div>
    );
  }
}

export default NavigationBar;
