import React from 'react';
import logo from './logo.svg';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Error page and other page components
import NoMatch from "./components/NoMatch";
import NavigationBar from "./components/Layout/NavigationBar";
import HomePage from "./components/Home";
import EligibilityPage from "./components/Eligibility";
import PricingPage from "./components/Pricing";
import ContactPage from "./components/Contact";
import AboutPage from "./components/About";
import myAccountPage from "./components/MyAccount/MyAccount";
import Footer from "./components/Layout/Footer"
import ResetPasswordLink from "./components/Forms/ResetPasswordLink";
import ResetPassword from "./components/Forms/ResetPassword";

// Help in routing
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import ProtectedRoute from "./components/Layout/ProtectedRoute";

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import axios from "axios";

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      user: {},
      redirectTo: null,
    };
  }

  componentDidMount() {
    // get the current logged in user from api/currentLogin
    const url = "http://localhost:5000/currentLogin"
    const data = {}
    axios(
      {
          method: 'POST',
          url,
          data: data,
          config: {
              headers: {
                  'Content-Type': 'application/json',
              }
          },
          withCredentials: true
      })
      .then(response => {
          console.log("currentLogin: ", response);
          
          if (response.data.status) {
            this.setState({loggedIn: true, user: response.data})
          } else if (this.state.loggedIn) {
            this.setState({ loggedIn: false, user: {}})
          }
          
      })
      .catch(error => {
          console.log("currentLogin error: ", error);
      }
    );
  }

  notify = (info, durInMilliSec) => {
    toast.notify(info, {duration: durInMilliSec});
  }
  
  logIn = (data) => {
    this.setState({ user: data, loggedIn: true, redirectTo: "/myAccount/profile" });
  }

  logOut = () => {
    this.setState({ user: {}, loggedIn: false, redirectTo: "/" });
    this.notify("Successfully Logged Out", 4000);
  }

  renderRedirect = () => {
    if (this.state.redirectTo !== null) {
      return (
          <Redirect to={{ pathname: this.state.redirectTo }} />
      );
    }
  }

  redirectToPath = (pathname, fromDeleteAc) => {
    this.setState({ redirectTo: pathname });
    if (fromDeleteAc){
      this.logOut()
    }
  }
  
  render() {
    
    return (
      <React.Fragment>
        
        <Router>
          <NavigationBar logIn={this.logIn} logOut={this.logOut} user={this.state.user} loggedInStatus={this.state.loggedIn}/>
          {this.renderRedirect()}
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/eligibility" component={EligibilityPage} />
            <Route exact path="/pricing" component={PricingPage} />
            <Route exact path="/contact" component={ContactPage} />
            <Route exact path="/about" component={AboutPage} />
            <Route exact path="/resetPassword" 
              render={(props) => <ResetPasswordLink redirectToPath={this.redirectToPath}/>}
            />
            <Route exact path="/resetPassword/:token" 
              render={(props) => <ResetPassword redirectToPath={this.redirectToPath}/>}
            />
            <Route exact path="/404" component={NoMatch} />
            <ProtectedRoute 
              pathName="/:myAccount/:details" 
              component={myAccountPage} 
              logIn={this.logIn}
              loggedIn={this.state.loggedIn}
              redirectToPath={this.redirectToPath}
            />
          </Switch>
        </Router>

        <div className="App">
          <header className="App-header">
            
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </header>
        </div>

        <Footer />

      </React.Fragment>
    );
  }
}

export default App;
