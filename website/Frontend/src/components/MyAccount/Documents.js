import React from 'react';

import { Col, Row, Container } from "react-bootstrap";

import toast from 'toasted-notes';
import 'toasted-notes/src/styles.css';
import axios from "axios";

import DisplayFiles from './DisplayFiles';
import UploadDocsForm from '../Forms/UploadDocsForm';

class DocumentsPage extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
          user: {},
          loggedIn: false,
      };
    }

    componentDidMount() {
        // get the current logged in user from api/currentLogin
        const url = "http://localhost:5000/currentLogin"
        const data = {}
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                },
                withCredentials: true
            })
            .then(response => {

                if (response.data.status) {
                    this.setState({loggedIn: true, user: response.data})
                } else if (this.state.loggedIn) {
                    this.setState({ loggedIn: false, user: {}})
                }
                
            })
            .catch(error => {
                console.log("currentLogin error: ", error);
            }
        );
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    onClickUpload = (data) => {
        this.setState({ user: data })
    }

    render() {
        
        return (
            <div>
                <Row>
                    <Col style={{ paddingTop: "2%" }}>
                        <header>
                            <h1 className="text-dark text-center">Your Documents</h1>
                        </header>
                    </Col>
                </Row>

                <Container>

                    <DisplayFiles currentUser={this.state.user} onClickUpload={this.onClickUpload}/>

                    <UploadDocsForm onClickUpload={this.onClickUpload}/>

                </Container>
            </div>
        );
    }
}

export default DocumentsPage;