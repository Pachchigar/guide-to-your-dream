import React from 'react';

import LoginForm from "../Forms/LoginForm";

// To load the popup screen for login page
import { Modal } from 'react-responsive-modal';
import { Route, Redirect, Switch } from "react-router-dom";

class ProtectedRoute extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        redirectTo: null,
      };
    }
     
    onCloseModal = () => {
        this.setState({ redirectTo: "/" });
    };

    onLogin = (data) => {
        this.props.logIn(data);
    }

    render() {
        if (this.state.redirectTo !== null) {
            return <Redirect to={{ pathname: this.state.redirectTo }} />
        }
        
        return (
            <Switch>
                <Route
                    exact 
                    path={this.props.pathName}
                    render={(props) => (
                        this.props.loggedIn === true
                        ?   <this.props.component redirectToPath={this.props.redirectToPath}/>
                        :   (<Modal open={true} onClose={this.onCloseModal} center>
                                <LoginForm onLogin={this.onLogin}/>
                            </Modal>)
                    )}
                /> 
                <Redirect to={{ pathname: "/404" }} />
            </Switch>
        );
        
    }
}

export default ProtectedRoute;