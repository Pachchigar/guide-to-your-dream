import React from 'react';

import { withRouter } from "react-router";

import ProfilePage from "./Profile";
import DocumentsPage from "./Documents";
import MasterDocumentsPage from "./MasterDocuments";
import NoMatch from "../NoMatch";

class MyAccount extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        component: null
      };
    }

    componentDidMount(){
        if (this.props.match.params.myAccount === "myAccount") {
            if (this.props.match.params.details === "profile") {
                this.setState({component: ProfilePage})
            } else if (this.props.match.params.details === "documents") {
                this.setState({component: DocumentsPage})
            } else if (this.props.match.params.details === "masterDocuments") {
                this.setState({component: MasterDocumentsPage})
            }
        }
    }

    render() {
        if (this.state.component !== null) {
            return (
                <this.state.component redirectToPath={this.props.redirectToPath}/>
            );
        } else {
            return <NoMatch />
        }
    }
}

export default withRouter(MyAccount);