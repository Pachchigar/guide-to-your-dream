from flask import Blueprint

main = Blueprint('main', __name__)

@main.route("/", methods=["GET", "POST", "OPTIONS"])
def home():
    return "API endpoint for VisaHelper.com"