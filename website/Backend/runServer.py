# from Application import create_app
from Application import app

# could pass in a config class as a parameter -- currently its using default for this application
# app = create_app()

if __name__ == "__main__":
    app.run(debug=True)