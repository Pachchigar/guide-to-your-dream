# Guide To Your Dream

The idea is to help an immigration consultant firm by providing a website.
Here, a client can register/log in to their portal, upload/update the necessary documents securely and personalize their profile.
There is also a master account whose documents page can query and get any registered user's documents

Functionaly done: Login|Register, forgot password --send reset link in email, logout, /contact, /myAccount/profile, /myAccount/documents, /myAccount/masterDocuments