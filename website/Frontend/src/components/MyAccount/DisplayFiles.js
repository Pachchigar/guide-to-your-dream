import React from 'react';
import { Table, Container } from "react-bootstrap";

import "../../css/DisplayFiles.css";

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';

import axios from "axios";

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

class DisplayFiles extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
      };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }


    makeDeleteRequest = (url, infoFrom, file_id) => {

        const data = {
            "file_id": file_id
        }
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'content-type': "application/json",
                    }
                },
                withCredentials: true
            })
            .then((response) => {
                console.log("Post "+infoFrom+": ", response);

                if (response.data.status) {
                    this.notify(infoFrom+" successfully!", 3000);
                    this.props.onClickUpload(response.data)
                } else {
                    // notify appropriate msg
                    this.notify(response.data.message, 6000);
                }
            })
            .catch(error => {
                console.log("Post "+infoFrom+" error: ", error);

                this.notify("Error during "+infoFrom, 4000);
            }
        );
    }

    deleteDoc = (file_id) => {
        const url = "http://localhost:5000/deleteDocument"
        confirmAlert({
            title: 'Confirm to Delete',
            message: "Are you sure to delete the file? This action can't be undone. Press Esc to cancel",
            buttons: [
              {
                label: 'Yes',
                onClick: () => this.makeDeleteRequest(url, "Delete Document", file_id)
              }
            ]
        });
    }


    makeDownloadRequest = (url, infoFrom, file_id, user_file) => {

        const data = {
            "file_id": file_id
        }
        axios(
            {
                method: 'POST',
                url,
                data: data,
                responseType: 'arraybuffer',
                config: {
                    headers: {
                        'content-type': "application/json",
                        'Accept': 'application/pdf'
                    }
                },
                withCredentials: true
            })
            .then((response) => {
                // 1. Create blob link to download
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', user_file.file_name_to_display);

                // 2. Append to html page
                document.body.appendChild(link);

                // 3. Force download
                link.click();

                // 4. Clean up and remove the link
                link.parentNode.removeChild(link);
                this.notify(infoFrom+" successfully!", 10000);
            })
            .catch(error => {
                console.log("Post "+infoFrom+" error: ", error);

                this.notify("Error during "+infoFrom, 4000);
            }
        );
    }


    downloadDocument = (file_id, user_file) => {
        const url = "http://localhost:5000/downloadDoc"
        this.makeDownloadRequest(url, "Download Document", file_id, user_file)
    }


    display = () => {
        const files = this.props.currentUser.files
        
        if (files && Object.keys(files).length !== 0) {
            return (
                Object.keys(files).map((file_id) => (
                    <tr key={files[file_id].id}>
                        <td>{files[file_id].file_description}</td>
                        <td 
                            className="link"
                            onClick={() => this.downloadDocument(file_id, files[file_id])}
                        >
                            {files[file_id].file_name_to_display}
                        </td>
                        <td>{files[file_id].date_created + " CST"}</td>
                        <td 
                            className="btn delete-color"
                            onClick={() => this.deleteDoc(file_id, files[file_id])}
                        >
                            Delete
                        </td>
                    </tr>
                ))
            );
        } else {
            return (
                <tr className="text-center">
                    <td>No files found</td>
                    <td>No files found</td>
                    <td>No files found</td>
                    <td>No files found</td>
                </tr>
            );
        }        
    }



    render() {    
        return (
            <Container>
                <Table
                    striped
                    hover
                    style={{
                        marginTop: "2%",
                        boxShadow: "0px 0px 20px -10px blue",
                        marginBottom: "4%"
                    }}
                >
                    <thead >
                        <tr>
                        <th>File Description</th>
                        <th>Name</th>
                        <th>Last Updated</th>
                        <th>Update</th>
                        </tr>
                    </thead>

                    <tbody>
                        {/* table headings */}
                        <this.display />
                    </tbody>
                </Table>
            </Container>
        );
    }
}

export default DisplayFiles;