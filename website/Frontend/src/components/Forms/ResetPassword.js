import React from 'react';

import axios from "axios";
import { Container, Button } from 'react-bootstrap';

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import { withRouter } from "react-router";

class ResetPassword extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
      };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    makePostRequest = (data, url) => {
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                },
                withCredentials: true
            })
            .then(response => {
                console.log("Post Password Reset: ", response);
                
                if (response.data.status) {
                    this.notify("Password Reset successful!", 3000);
                    // redirect to home
                    this.props.redirectToPath("/");
                } else {
                    this.notify("Password Reset Unsuccessful: " + response.data.message, 6000);
                }
                
            })
            .catch(error => {
                console.log("Post Password Reset error: ", error);

                this.notify("Error during Password Reset", 4000);
            }
        );
    }

    resetSubmitHandler = (event) => {
        // make the POST API call and pass on the response.data as a 
        // parameter to the props function
        event.preventDefault();
        if (event.target.password.value !== "" || event.target.confirmation_password.value !== "") {
            if (!/([A-Z]+)/g.test(event.target.password.value) ||
                !/([a-z]+)/g.test(event.target.password.value) || 
                event.target.password.value.length < 8 ) {
                this.notify("Allowed passwords include: "+
                    "At least 8 characters, one upper case, one lower case, letters and/or numbers",
                    null);
                this.notify("Invalid Password", 4000);
            } else {
                if (event.target.password.value === event.target.confirmation_password.value) {
                    const data = {
                        password: event.target.password.value
                    }
                    const url = "http://localhost:5000/resetPassword/"+this.props.match.params.token
        
                    // to log in the user upon successful registration
                    this.makePostRequest(data, url);
                } else{
                    this.notify("Confirmation Password does not match", 4000);
                }
            }
        } else {
            this.notify("Password field can't be empty.", 3000)
        }
    }

    render() {
        return (
            <Container>
                <form name="resetPasswordForm"
                    className="active-cyan-4 my-2"
                    onSubmit={this.resetSubmitHandler}
                >

                    {/* add email textarea and submit button */}
                    <h2 style={{textAlign: "center"}}>Reset Password</h2>

                    <label className="form-inline" style={{marginTop: "15px"}}>
                        New Password:
                        <input 
                            name="password"
                            className="form-control" 
                            style={{ boxShadow: "5px 5px 15px -12px black", margin: "0px 0px 0px 81px" }}
                            type="password" 
                            placeholder="********" 
                            onChange={this.handleChange}
                        />
                    </label>

                    <label className="form-inline" style={{marginTop: "15px"}}>
                        Confirm New Password:
                        <input 
                            name="confirmation_password"
                            className="form-control" 
                            style={{ boxShadow: "5px 5px 15px -12px black", margin: "0px 0px 0px 20px" }}
                            type="password" 
                            placeholder="********" 
                            onChange={this.handleChange}
                        />
                    </label>

                    <Button
                        type="submit"
                        variant="outline-secondary"
                        style={{ marginLeft: "120px", marginTop: "10px",textAlign: "center"}}
                    >
                        Reset Password
                    </Button>
                
                </form>
            </Container>
        );
    }
}

export default withRouter(ResetPassword);