import React from 'react';

class LoginFields extends React.Component {

    render() {
        var username_value = ""

        if (this.props.username_value) {
            username_value = this.props.username_value
        }

        return (
            <>
            <label>
                UserName:
                <input 
                    name="username"
                    className="form-control" 
                    style={{ boxShadow: "5px 5px 15px -12px black", margin: "10px 0px 0px 55px" }}
                    type="email" 
                    placeholder="Enter your email" 
                    onChange={this.handleChange} 
                    defaultValue={username_value}
                />
            </label>
            
            <label>
                Password:
                <input 
                    name="password"
                    className="form-control" 
                    style={{ boxShadow: "5px 5px 15px -12px black", margin: "20px 0px 0px 62px " }}
                    type="password" 
                    placeholder="*******" 
                    onChange={this.handleChange} 
                />
            </label>
            </>
        );
    }
}

export default LoginFields;