from flask import request
from Application.models import User
from Application import mail, db
from flask_mail import Message
import pprint

pp = pprint.PrettyPrinter(indent=4)


def send_contact_email(inquiry, recipient_email, toType):
    try:
        subject = ''
        if (toType == "user"):
            subject = 'Inquiry Confirmation'
        else:
            subject = 'New Inquiry'

        msg = Message(subject, 
                    sender='no-reply@visahelp.com', 
                    recipients=[recipient_email])
        
        if (toType == "user"):
            msg.body = f'''An email with the following inquiry was sent to one of our team members:

"{inquiry}"

- Please allow upto 2 business days to respond.
- If not already, register with us (http://localhost:3000/myAccount/profile) and get a whatsapp contact number on your profile page for a quicker response!
- Until then, explore our website (http://localhost:3000/) and find questions to ask in order for us to help you achieve your dream of going to Canada!

If you did not make this request then simply ignore this email.
'''
        else:
            user = get_user_from_email(recipient_email)

            starting = "perspective"
            first_name = ""
            last_name = ""
            contact = ""
            if (user):
                starting = "current"
                first_name = user.first_name
                last_name = user.last_name
                contact = user.contact

            msg.body = f'''A {starting} user sent an inquiry!

from    : {first_name, last_name, recipient_email, contact}
Inquiry : "{inquiry}"
'''
        mail.send(msg)
        return True
    except Exception:
        return False



def get_user_from_email(email):
    user = db.session.query(User).filter_by(username=email).first()
    return user