import React from 'react';

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import { Button, Container } from "react-bootstrap";

import axios from "axios";

class ContactUsForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    makePostRequest = (data) => {
        const url = "http://localhost:5000/contactUs"
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'content-type': 'application/json',
                    }
                },
                withCredentials: true
            })
            .then(response => {
                console.log("Post Send inquiry: ", response);
                
                if (response.data.status) {
                    this.notify("Provided E-mail should recieve a confirmation email for submitting your inquiry", 10000)
                } else {
                    this.notify(response.data.message, 6000);
                }
            })
            .catch(error => {
                console.log("Post Send Inquiry error: ", error);

                this.notify("Error during Send Inquiry", 4000);
            }
        );
    }

    sendInquiryEmail = (event) => {
        
        event.preventDefault();
        
        if (event.target.emailAddress.value === "" || event.target.emailAddressVerification.value === "") {
            this.notify("Email Address can't be empty", 4000)
        } else if (event.target.emailAddress.value !== event.target.emailAddressVerification.value) {
            this.notify("Verify your email", 4000)
        } else if (event.target.inquiry.value === "") {
            this.notify("Enter your question before sending the inquiry", 4000)
        } else {

            const data = {
                user_email_address: event.target.emailAddress.value,
                inquiry: event.target.inquiry.value
            }
            
            // make the post request to send the email to team member and/or to send a confirmation email to the provided email.
            this.makePostRequest(data);
        }

        // reset the form
        event.target.emailAddress.value = ''
        event.target.emailAddressVerification.value = ''
        event.target.inquiry.value = ''
    }

    render() {
        var user_email = ''
        if (this.props.loggedIn){
            user_email = this.props.user.username
        }
        return (
            <div>
                <Container>
                    <form name="contactUsForm"
                        className="border rounded-lg active-cyan-4 my-2"
                        onSubmit={this.sendInquiryEmail}
                        style={{padding: "20px 0px 20px 20px" }}
                    >

                        <label className="form-inline">
                            E-mail: 
                            <input 
                                className="form-control"
                                name="emailAddress"
                                type="email" 
                                placeholder="Enter your email address" 
                                style={{ width: "30%", boxShadow: "5px 5px 15px -12px black", margin: "10px 47px 10px 77px" }}
                                defaultValue={user_email}
                            /> 
                        </label>

                        <label className="form-inline">
                            Re-enter Email: 
                            <input 
                                className="form-control"
                                name="emailAddressVerification"
                                type="email" 
                                placeholder="Enter your email address" 
                                style={{ width: "30%", boxShadow: "5px 5px 15px -12px black", margin: "10px 0px 10px 17px" }}
                                defaultValue={user_email}
                            /> 
                        </label>

                        <label className="form-inline">
                            Your Inquiry: 
                            <textarea 
                                className="form-control"
                                name="inquiry"
                                type="text" 
                                placeholder="Enter your inquiry or questions that you want us to answer at any point of time!" 
                                style={{ width: "80%", boxShadow: "5px 5px 15px -12px black", margin: "10px 0px 10px 35px" }}
                            /> 
                        </label>

                        <Button
                            type="submit"
                            variant="outline-primary"
                            style={{ marginLeft: "80%", marginTop: "10px",textAlign: "center"}}
                        >
                            Send email
                        </Button>

                    </form>
                </Container>

                <p style={{marginTop: "50px"}}>Note: 
                <br/>1. No information other than your E-mail id is needed for making an inquiry.
                <br/>2. One of our team members will reach out at the given email within 2 business days.
                <br/>3. If not already, register with us and get a whatsapp contact number on your profile page for a quicker response!
                <br/>4. Until then, explore our website and find questions to ask in order for us to help you achieve your dream of doing to Canada!</p>

            </div>
        );
    }
}

export default ContactUsForm;