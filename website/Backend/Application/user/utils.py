from flask import request, Response
from Application import db, bcrypt, mail, app
from Application.models import User
from flask_login import current_user
import secrets
from werkzeug import secure_filename
import os
from PIL import Image
from flask_mail import Message
import json
import pprint

pp = pprint.PrettyPrinter(indent=4)

def validPassword (password, confirmationPassword):
    if (password == confirmationPassword):
        return True
    return False


def validusername (username):
    if (db.session.query(User).filter_by(username=username).first()):
        return False
    return True


def validContact (contact):
    if (db.session.query(User).filter_by(contact=contact).first()):
        return False
    return True


def updateDB(content, user, msg):
    if (validResetFirstName(content.get("first_name"), user.first_name) or
        validResetLastName(content.get("last_name"), user.last_name) or
        validResetContact(content.get("contact"), user.contact, msg) or
        validResetUsername(content.get("username"), user.username, msg) or
        validResetPassword(content.get("password"), user.password, content.get("password_confirmation")) or
        validResetUserImage(msg)
        ):
        db.session.commit()
        return True
    return False


def validResetFirstName (newFirstName, oldFirstName):
    if(not newFirstName == oldFirstName):
        # update first_name in DB
        current_user.first_name = newFirstName
        return True
    return False



def validResetLastName (newLastName, oldLastName):
    if(not newLastName == oldLastName):
        # update first_name in DB
        current_user.last_name = newLastName
        return True
    return False



def validResetContact (newContact, oldContact, msg) :
    if(not ( newContact == oldContact)):
        if validContact(newContact):
            # update contact in DB
            current_user.contact = newContact
            return True
        else: 
            msg.append("Contact already exists. Choose a different one")
    return False



def validResetUsername (newUsername, oldusername, msg):
    if(not (newUsername == oldusername)):
        if validusername(newUsername):
            # update username in DB
            current_user.username = newUsername
            return True
        else: 
            msg.append("username already exists. Choose a different one")
    return False



def validResetPassword (newPassword, oldPassword, passwordConfirmation):
    # update password if not empty and not same as the one stored in db
    if(newPassword != "" and (not bcrypt.check_password_hash(oldPassword, newPassword)) and 
        validPassword(newPassword, passwordConfirmation) ):
        # update username in DB
        current_user.password = bcrypt.generate_password_hash(newPassword).decode('utf-8')
        return True
    return False



def validResetUserImage (msg):
    if(request.files.get("user_image")):
        if allowed_file(request.files.get("user_image").filename, ALLOWED_FILE_EXTENSIONS):
            # store the file in public folder
            filename = saveProfilePic(request.files.get("user_image"))
            if (allowed_file_name(filename)):
                current_user.user_image = filename
                return True
            else:
                msg.append("Error in uploading Profile Pic, try again")
        else: 
            msg.append("File type must be 'png', 'jpg' or 'jpeg'")
    return False



ALLOWED_FILE_EXTENSIONS = {'png', 'jpg', 'jpeg'}



def allowed_file(filename, allowedExtensions):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in allowedExtensions



def saveProfilePic(profilePic):
    filename = secure_filename(profilePic.filename)
    random_hex = secrets.token_hex(8)
    _, ext = os.path.splitext(profilePic.filename)
    filename = random_hex + ext
    # pp.pprint(app.root_path + 'static/Profile_Pictures' + filename)
    picture_path = os.path.join(app.root_path, 'static/Profile_Pictures', filename)

    outputSize = (300, 300)
    image_thumbnail = Image.open(profilePic)
    image_thumbnail.thumbnail(outputSize)
    image_thumbnail.save(picture_path)
    return filename



def allowed_file_name(filename):
    if db.session.query(User).filter_by(user_image=filename).first():
        return False
    return True



def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password Reset Requested', 
                sender='no-reply@visahelp.com', 
                recipients=[user.username])
    
    msg.body = f'''To reset your password, visit the following link:

The link will expire in 30 mins:
http://localhost:3000/resetPassword/{token}

If you did not make this request then simply ignore this email and no changes will be made.
'''
    mail.send(msg)


# takes in an object of dictionary
def response(obj):
    response = Response(json.dumps(obj), status="200")
    response.status_code = 200
    response.headers.add("content-type", "application/json")

    return response




def saveDocument (docFile):
    filename = secure_filename(docFile.filename)
    random_hex = secrets.token_hex(8)
    _, ext = os.path.splitext(filename)
    filename = random_hex + ext
    
    doc_path = os.path.join(app.root_path, 'static/Documents', filename)
    docFile.save(doc_path)
    return filename




def getUserFiles (user_files):
    files={}
    for user_file in user_files:
        file_details = {}
        file_details["id"] = user_file.id
        file_details["file_description"] = user_file.file_description
        file_details["file_name"] = user_file.file_name
        file_details["file_name_to_display"] = user_file.file_name_to_display
        file_details["date_created"] = str(user_file.date_created)
        file_details["user_id"] = user_file.user_id
        files[user_file.id] =  file_details
    return files