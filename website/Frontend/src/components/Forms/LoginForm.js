import React from 'react';

import 'react-responsive-modal/styles.css';

import LoginFields from "./LoginFields";
import RegistrationFields from "./RegistrationFields";


import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import 'react-tabs/style/react-tabs.css';
import { Redirect } from "react-router-dom";

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import axios from "axios";

import Validate from "./Validation/ValidateFields"

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tabIndex: 0,
        };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    makePostRequest = (data, infoFrom, url) => {
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                },
                withCredentials: true
            })
            .then(response => {
                console.log("Post " + infoFrom + ": ", response);
                
                if (response.data.status) {
                    this.notify(infoFrom + " successful!", 3000);
                    this.props.onLogin(response.data);
                } else {
                    this.notify(infoFrom + " Unsuccessful: " + response.data.message, 6000);
                }
                
            })
            .catch(error => {
                console.log("Post "+ infoFrom +" error: ", error);

                this.notify("Error during "+ infoFrom, 4000);
            }
        );
    }

    registrationSubmitHandler = (event) => {
        // make the POST API call and pass on the response.data as a 
        // parameter to the props function
        event.preventDefault();
        if (Validate.validateRegistrationInputValues(event, true)) {
            const data = {
                first_name: event.target.first_name.value,
                last_name: event.target.last_name.value,
                contact: event.target.contact.value,
                username: event.target.username.value,
                password: event.target.password.value,
                password_confirmation: event.target.password_confirmation.value
            }
            const url = "http://localhost:5000/register"
    
            // to log in the user upon successful registration
            this.makePostRequest(data, "Registration", url);
        }
    }

    addRegistrationForm = () => {
        return (
            <form name="registrationform"
                className="form-inline active-cyan-4 my-2"
                onSubmit={this.registrationSubmitHandler}
            >

            <div>

                <RegistrationFields />

                <div style={{textAlign: "center"}}>
                    <input 
                        className="btn-primary "
                        style={{ boxShadow: "5px 5px 30px -10px black", marginTop: "10px" }}
                        type="submit" 
                        value="Register" 
                    />
                </div>
            </div>
            
            </form>
        );
    }

    loginSubmitHandler = (event) => {
        // make the POST API call and pass on the response.data as a 
        // parameter to the props function
        event.preventDefault();
        if (Validate.validateLoginInputValues(event, true, false)) {
            const data = {
                username: event.target.username.value,
                password: event.target.password.value
            }
            const url = "http://localhost:5000/login"
    
            // to log in the user upon successful registration
            this.makePostRequest(data, "Login", url);
        }
    }

    addLoginForm = () => {
        return (
            
            <form name="loginform"
                className="form-inline active-cyan-4 my-2"
                onSubmit={this.loginSubmitHandler}
            >

            <div>
                {/* add email and password textareas */}
                <LoginFields />
                <div style={{textAlign: "center"}}>
                    <input
                        className="btn-primary "
                        style={{ boxShadow: "5px 5px 30px -10px black", margin: "20px 10px 0px 0px" }}
                        type="submit" 
                        value="Login" 
                    />
                </div>

                <div style={{ textAlign: "center", padding: "10px 0 0 0"}}>
                    <a href="/resetPassword" >Forgot Password?</a>
                </div>
            </div>
            
            </form>
        );
    }

    renderContent = () => {
        const tabTitles = ["Login", "Register"];
        const title = tabTitles[this.state.tabIndex];
        if (title === "Login") {
            return <this.addLoginForm />;
        } else if (title === "Register") {
            return <this.addRegistrationForm />;
        } else {
            return <Redirect to={{ pathname: "/404" }} />
        }
    }

    addTab = () => {
        return (
            <Tabs
                selectedIndex={this.state.tabIndex}
                onSelect={(tIndex) => this.setState({ tabIndex: tIndex })}
            >
                <TabList>
                    <Tab>Login</Tab>
                    <Tab>Register</Tab>
                </TabList>
                <TabPanel></TabPanel>
                <TabPanel></TabPanel>
            </Tabs>
        );
    }

    myForm = () => {
        return (
            <>
                <this.addTab />
                <this.renderContent />
            </>
        );
    }

    render() {
        return (
            <div>
                <this.myForm /> 
            </div>
        );
    }
}

export default LoginForm;