import LoginInputValues from "./LoginInputValues"
import RegistrationInputValues from "./RegistrationInputValues"

let validateLoginInputValues = (event, fromLogin, fromRegister) => {
    return LoginInputValues.validate(event, fromLogin, fromRegister)
}

let validateRegistrationInputValues = (event, fromRegister) => {
    return RegistrationInputValues.validate(event, fromRegister)
}

export default {validateLoginInputValues, validateRegistrationInputValues};