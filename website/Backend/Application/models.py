from Application import db, login_manager, app
from datetime import datetime
from flask_login import UserMixin
# from flask import current_app

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User (db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(20), nullable=False)
    last_name = db.Column(db.String(20), nullable=False)
    contact = db.Column(db.String(15), unique=True, nullable=False)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    user_image = db.Column(db.String(20), nullable=False, default='default.jpg')
    files = db.relationship('File', backref='owner', lazy=True)

    def get_reset_token (self, expires_secs=1800) :
        s = Serializer(app.config['SECRET_KEY'], expires_secs)
        return s.dumps({ "user_id": self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User('{self.id}','{self.user_image}','{self.first_name}','{self.last_name}','{self.contact}', '{self.username}')"

class File (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    file_description = db.Column(db.String(120), nullable=False)
    file_name = db.Column(db.String(), nullable=False)
    file_name_to_display = db.Column(db.String(120), nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
            return f"File('{self.id}', '{self.file_name}','{self.date_created}')"
