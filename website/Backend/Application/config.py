import os

class Config:
    # for cookie and security
    # SECRET_KEY = os.environ.get("PROJECT_SECRET_KEY")
    SECRET_KEY = "cd4fee8e14277eb316ba2fcaccc3dc0d"

    # craetes a local file that has the sqlite database
    # SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_DATABASE_URI = "sqlite:///database.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    MAIL_SERVER = "smtp.gmail.com"
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    # replace os.environ with actual userName and password
    MAIL_USERNAME = os.environ.get("EMAIL_USER")
    MAIL_PASSWORD = os.environ.get("EMAIL_PASS")