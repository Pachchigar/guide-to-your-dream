import React, { Component } from 'react';

class NoMatch extends Component {
  render() {
    return (
      <h1 className="text-dark text-center" style={{marginTop: '50px'}}>
        404 Error: This page is not available.
      </h1>
    );
  }
}

export default NoMatch;