import React from 'react';

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import { Button, Container } from "react-bootstrap";

import axios from "axios";

class UploadDocsForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            docFile: null,
        };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    makePostRequest = (data, infoFrom, url, contentType) => {
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'content-type': contentType,
                    }
                },
                withCredentials: true
            })
            .then(response => {
                console.log("Post " + infoFrom + ": ", response);
                
                if (response.data.status) {
                    // this.notify(response.data.message, 6000);
                    this.notify(infoFrom + " successful!", 3000);
                    this.props.onClickUpload(response.data)
                } else {
                    this.notify(response.data.message, 6000);
                }
            })
            .catch(error => {
                console.log("Post "+ infoFrom +" error: ", error);

                this.notify("Error during "+ infoFrom, 4000);
            }
        );
    }

    uploadHandler = (event) => {
        
        event.preventDefault();

        if (event.target.file_description.value === "") {
            this.notify("Description field can't be empty")
        } else if (this.state.docFile === null) {
            this.notify("No File selected to uplaod")
        } else {
            const data = {
                file_description: event.target.file_description.value
            }
            const url = "http://localhost:5000/uploadDoc"
    
            var documentData = new FormData();
            documentData.append("data", JSON.stringify(data))
            documentData.append("docFile", this.state.docFile)

            // reset the form
            event.target.file_description.value = ''
            event.target.docFile.value = ''

            // to update user details in the db
            this.makePostRequest(documentData, "Upload Document", url, 'multipart/form-data');
        }

    }

    handleDocUpload = (event) => {
        if (event.target.files.length) {
            this.setState({ docFile: event.target.files[0]})
        }
    }

    render() {
        return (
            <div>
            <Container className="text-center">
                <form name="uploadDocsForm"
                    className="active-cyan-4 my-2"
                    onSubmit={this.uploadHandler}
                >

                <label>
                    <input 
                        name="file_description"
                        type="text" 
                        placeholder="Enter Brief description about the file" 
                        style={{width: "290px", borderRadius: "5px", border: "1px solid #ccc", padding: "8px"}}
                    /> 
                    
                    <input
                        name="docFile"
                        type="file" 
                        style={{marginLeft: "20px"}}
                        onChange={this.handleDocUpload}
                    />

                    <Button
                        variant="outline-secondary"
                        type="submit" 
                    >
                        Upload
                    </Button>
                </label>
                </form>
            </Container>
            <p style={{marginTop: "50px"}}>Note: 
            <br/>1. Upload only the documents as guided by one of our team members personally.
            <br/>2. If you haven't already contacted them, do so through 
                our <a href="http://localhost:3000/contact">Contact Us page</a> or via 
                whatsapp at <a href="https://web.whatsapp.com/" 
                                target="_blank"
                                rel="noopener noreferrer">+1 (512) 806 9375</a>
            <br/>3. If you haven't heard from them yet, no need to uplaod any documents until then. Good Luck!</p>
            </div>
        );
    }
}

export default UploadDocsForm;