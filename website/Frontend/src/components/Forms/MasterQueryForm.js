import React from 'react';

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import { Button, Container } from "react-bootstrap";

import axios from "axios";

class UploadDocsForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            docFile: null,
        };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    makePostRequest = (data, url) => {
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'content-type': 'application/json',
                    }
                },
                withCredentials: true
            })
            .then(response => {
                console.log("Post Master query: ", response);
                
                if (response.data.status) {
                    // this.notify(response.data.message, 6000);
                    this.notify("Master Query successful!", 3000);
                } else {
                    this.notify(response.data.message, 6000);
                }
                this.props.onClickUpload(response.data)
            })
            .catch(error => {
                console.log("Post Master Query error: ", error);

                this.notify("Error during Master Query", 4000);
            }
        );
    }

    queryHandler = (event) => {
        
        event.preventDefault();

        if (event.target.username.value === "" && event.target.contact.value === "") {
            this.notify("Both query fields can't be empty")
        } else if (event.target.username.value === "" && event.target.contact.value.length < 10) {
            this.notify("Contact number can't be less than 10 digits")
        } else {
            const data = {
                username: event.target.username.value,
                contact: event.target.contact.value
            }
            const url = "http://localhost:5000/masterDocuments"

            // reset the form
            event.target.username.value = ''
            event.target.contact.value = ''

            // to update user details in the db
            this.makePostRequest(data, url);
        }
    }

    render() {
        return (
            <div>
            <Container className="text-center">
                <form name="MasterQueryForm"
                    className="active-cyan-4 my-2"
                    onSubmit={this.queryHandler}
                >

                <label className="form-inline border rounded-lg" 
                    style={{paddingLeft: "20px", marginTop:"20px", marginBottom: "40px"}}
                >
                    Username: 
                    <input 
                        className="form-control"
                        name="username"
                        type="email" 
                        style={{margin: "20px 25px 20px 10px", width: "260px", boxShadow: "5px 5px 15px -12px black"}}
                        placeholder="Enter a user's username" 
                    /> 

                    <label style={{marginRight: "20px"}}>And / Or</label>
                    
                    Contact No.:
                    <input
                        className="form-control"
                        name="contact"
                        type="integer" 
                        style={{margin: "20px 25px 20px 10px", width: "260px", boxShadow: "5px 5px 15px -12px black"}}
                        placeholder="Enter a user's contact number"
                    />

                    <Button
                        variant="outline-secondary"
                        type="submit" 
                    >
                        query
                    </Button>
                </label>
                </form>
            </Container>
            </div>
        );
    }
}

export default UploadDocsForm;