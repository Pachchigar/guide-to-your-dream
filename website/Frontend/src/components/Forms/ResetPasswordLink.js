import React from 'react';

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import axios from "axios";
import { Container, Button } from 'react-bootstrap';

class ResetPasswordForm extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
      };
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    makePostRequest = (data, url) => {
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                },
                withCredentials: true
            })
            .then(response => {
                console.log("Post Reset link send: ", response);
                
                if (response.data.status) {
                    this.notify("A link with reset password instructions is sent to the E-mail successfully!", 5000);
                    // redirect to home
                    this.props.redirectToPath("/");
                } else {
                    this.notify("Reset link Unsuccessful: " + response.data.message, 6000);
                }
                
            })
            .catch(error => {
                console.log("Post Reset link send error: ", error);

                this.notify("Error during Reset link send", 4000);
            }
        );
    }

    resetSubmitHandler = (event) => {
        // make the POST API call and pass on the response.data as a 
        // parameter to the props function
        event.preventDefault();
        if (event.target.emailField.value !== "") {
            const data = {
                username: event.target.emailField.value
            }
            const url = "http://localhost:5000/resetPassword"

            // to log in the user upon successful registration
            this.makePostRequest(data, url);
        } else {
            this.notify("Email field can't be empty.", 3000)
        }
            
    }

    addForm = () => {
        return (
            <Container>
                <form name="resetPasswordEmailForm"
                    className="active-cyan-4 my-2"
                    onSubmit={this.resetSubmitHandler}
                >

                    {/* add email textarea and submit button */}
                    <h2 style={{textAlign: "center"}}>Forgot Password?</h2>
                    <p>1. Enter your registered E-mail below</p>
                    <p>2. If an account exists with the provided E-mail, an E-mail will be sent at the provided email with the reset password link</p>
                    <p>3. Follow the link to reset the password</p>

                    <label className="form-inline">
                        Email:
                        <input 
                            name="emailField"
                            className="form-control" 
                            style={{ boxShadow: "5px 5px 15px -12px black", margin: "0px 0px 0px 20px" }}
                            type="email" 
                            placeholder="Enter your email" 
                            onChange={this.handleChange}
                        />
                        <Button
                            type="submit"
                            variant="outline-secondary"
                            style={{ marginLeft: "20px"}}
                        >
                            Send Reset Link
                        </Button>
                    </label>
                
                </form>
            </Container>
        );
    }

    render() {
        return (
            <this.addForm /> 
        );
    }
}

export default ResetPasswordForm;