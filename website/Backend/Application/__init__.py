from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_login import LoginManager
from flask_mail import Mail
from Application.config import Config

app = Flask(__name__)
app.config.from_object(Config)

mail = Mail(app)
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)

from Application.user.routes import users
from Application.main.routes import main
from Application.files.routes import files
from Application.contactUs.routes import contactUs
app.register_blueprint(users)
app.register_blueprint(main)
app.register_blueprint(files)
app.register_blueprint(contactUs)

# def create_app (config_class=Config):
#     app = Flask(__name__)
#     app.config.from_object(Config)

#     mail.init_app(app)
#     db.init_app(app)
#     bcrypt.init_app(app)
#     login_manager.init_app(app)

#     # from Application import routes
#     from Application.user.routes import users
#     from Application.main.routes import main
#     from Application.files.routes import files
#     app.register_blueprint(users)
#     app.register_blueprint(main)
#     app.register_blueprint(files)

#     return app