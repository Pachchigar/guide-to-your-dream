import React from 'react';

import { Col, Row, Container, Figure, Button } from "react-bootstrap";
import "../../css/Profile.css";

import { Modal } from 'react-responsive-modal';

import ResetProfileForm from "../Forms/ResetProfileForm";

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';

import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

import axios from "axios";

class MyAccount extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
          user: {},
          openLoginModal: false,
          loggedIn: false,
      };
    }

    componentDidMount() {
        // get the current logged in user from api/currentLogin
        const url = "http://localhost:5000/currentLogin"
        const data = {}
        axios(
            {
                method: 'POST',
                url,
                data: data,
                config: {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                },
                withCredentials: true
            })
            .then(response => {

                if (response.data.status) {
                    this.setState({loggedIn: true, user: response.data})
                } else if (this.state.loggedIn) {
                    this.setState({ loggedIn: false, user: {}})
                }
                
            })
            .catch(error => {
                console.log("currentLogin error: ", error);
            }
        );
    }

    notify = (info, durInMilliSec) => {
        toast.notify(info, {duration: durInMilliSec});
    }

    tryImage = () => {
        var src = "/images/Profile_pics/default.jpg"

        if (this.state.user.user_image) {
            src = "/images/Profile_Pictures/"+this.state.user.user_image
        }
        
        return <Figure.Image
                    className="logo-hover"
                    src={src}
                    alt={this.state.user.first_name}
                    width={275}
                />
    }

    onOpenModal = () => {
        this.setState({ openLoginModal: true });
    };
     
    onCloseModal = (data) => {
        if (data) {
            this.setState({ user: data, openLoginModal: false });
        } else {
            this.setState({ openLoginModal: false });
        }
    };

    makeDeleteRequest = () => {
        const url = "http://localhost:5000/deleteAccount"
        axios(
            {
                method: 'POST',
                url,
                data: {},
                config: {
                    headers: {
                        'content-type': "application/json",
                    }
                },
                withCredentials: true
            })
            .then(response => {
                console.log("Post Deletion: ", response);
                
                if (response.data.status) {
                    this.notify("Account deleted successfully!", 3000);
                    this.props.redirectToPath("/", true);
                } else {
                    // notify appropriate msg
                    this.notify(response.data.message, 6000);
                }
            })
            .catch(error => {
                console.log("Post Deleting account error: ", error);

                this.notify("Error during deleting acount", 4000);
            }
        );
    }

    deleteAccount = () => {
        confirmAlert({
            title: 'Confirm to Delete',
            message: "Are you sure to delete the account? This action can't be undone. All account information will be lost. Press Esc to cancel",
            buttons: [
              {
                label: 'Yes',
                onClick: this.makeDeleteRequest
              },
              {
                label: 'Cancel',
                onClick: this.onCloseModal
              }
            ]
        });
    }

    render() {
        
        return (<div>
            <Container >
                <Row>
                    <Col>
                        <div className="profile_pic mx-auto">
                            {/* insert profile picture */}
                            {/* last option is to store it in the public folder: */}
                            {/* process.env.PUBLIC_URL + '/img/logo.png' */}
                            <this.tryImage />
                        </div>
                    </Col>

                    <Col className="user_details">
                        <h2>{this.state.user.first_name} {this.state.user.last_name}</h2>
                        <h5>{this.state.user.username}</h5>
                        <h5>{this.state.user.contact}</h5>
                        <Button
                            className="update_btn"
                            onClick={this.onOpenModal}
                            variant="outline-secondary"
                        >
                            Update profile
                        </Button>
                        <Button
                            style={{ marginLeft: "10px"}}
                            className="update_btn btn-danger"
                            onClick={this.deleteAccount}
                        >
                            Delete Account
                        </Button>
                    </Col>
                </Row>

                {/* <this.resetUserDetails /> */}
                <Modal open={this.state.openLoginModal} onClose={this.onCloseModal} center>
                    <ResetProfileForm user={this.state.user} onClickUpdate={this.onCloseModal}/>
                </Modal>
            </Container>

            {/* render reah us component */}
            <div className="footer text-center reachUs">
                <h6>Reach us via Whatsapp at <a 
                                                href="https://web.whatsapp.com/" 
                                                target="_blank" 
                                                rel="noopener noreferrer">+1 (512) 806 9375</a></h6>
            </div>
        </div>);
    }
}

export default MyAccount;